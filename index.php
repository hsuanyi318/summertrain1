<?php
  session_start();
  require_once("conn.php");
  require_once("utils.php");
  /*
    1. 從 cookie 裡面讀取 PHPSESSID(token)
    2. 從檔案裡面讀取 session id 的內容
    3. 放到 $_SESSION
  */
  $username = NULL;
  $user = NULL;
  if(!empty($_SESSION['username'])) {
    $username = $_SESSION['username'];
    $user = getUserFromUsername($username);
  }

  $stmt = $conn->prepare(
    'select '.
      'C.id as id, C.content as content, '.
      'C.created_at as created_at, U.nickname as nickname, U.username as username '.
    'from comments as C ' .
    'left join users as U on C.username = U.username '.
    
    'order by C.id desc'
  );
  $result = $stmt->execute();
  if (!$result) {
    die('Error:' . $conn->$error);
  }
  $result = $stmt->get_result();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>留言板</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  
  <main class="board">
      <div>
        
        <?php if (!$username) { ?>
          <a class="board__btn" href="register.php">註冊</a>
          <a class="board__btn" href="login.php">登入</a>
        <?php } else { ?>
          <a class="board__btn" href="logout.php">登出</a>
          <span class="board__btn update-nickname">編輯暱稱</span>
          <form class="hide board__nickname-form board__new-comment-form" method="POST" action="update_user.php">
            <div class="board__nickname">
              <span>新的暱稱：</span>
              <input type="text" name="nickname" />
            </div>
            <input class="board__submit-btn" type="submit" />
          </form>
          <h3>你好！<?php echo $user['nickname']; ?></h3>
          <h3>登入成功</h3>
        <?php } ?>
      </div>

      <h1 class="board__title">Comments</h1>
      <?php
        if (!empty($_GET['errCode'])) {
          $code = $_GET['errCode'];
          $msg = 'Error';
          if ($code === '1') {
            $msg = '資料不齊全';
          }
          echo '<h2 class="error">錯誤：' . $msg . '</h2>';
        }
      ?>
      
        <form class="board__new-comment-form" method="POST" action="handle_add_comment.php">
        <div class="board__nickname">
          <span>暱稱：</span>
          <input type="text" name="nickname" />
        </div>
          <textarea name="content" rows="5"></textarea>
          <?php if ($username) { ?>
            
            <input class="board__submit-btn" type="submit" />
            
          <?php } else { ?>
            <h3>請登入發布留言</h3>
          <?php } ?>  
        </form>
      <div class="board__hr"></div>
      <section>
        <?php while($row = $result->fetch_assoc()) { ?>
          <div class="card">
            <div class="card__avatar"></div>
            <div class="card__body">
                <div class="card__info">
                  <span class="card__author">
                    <?php echo $row['nickname']; ?>
                    (@<?php echo ($row['username']); ?>)
                  </span>
                  <span class="card__time">
                    <?php echo $row['created_at']; ?>
                  </span>
                  <?php if ($row['username'] === $username) { ?>
                    <a href="update_comment.php?id=<?php echo $row['id'] ?>">編輯</a>
                    <a href="delete_comment.php?id=<?php echo $row['id'] ?>">刪除</a>
                  <?php } ?>
                </div>
                <p class="card__content"><?php echo $row['content']; ?></p>
            </div>
          </div>
        <?php } ?>
      </section>
  </main>
  <script>
    var btn = document.querySelector('.update-nickname')
    btn.addEventListener('click', function() {
      var form = document.querySelector('.board__nickname-form')
      form.classList.toggle('hide')
    })
  </script>
</body>
</html>